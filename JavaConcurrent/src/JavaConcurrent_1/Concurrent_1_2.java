package JavaConcurrent_1;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Concurrent_1_2 {
    //用interrupt停止线程
    static class StopThread implements Runnable{

        @Override
        public void run() {
            int count = 0;
            while(!Thread.currentThread().isInterrupted() &&
            count < 1000){
                System.out.println("count = " + count++);
            }
        }
    }

    //为什么用volatile标记位的停止方法是错误的
    static class Producer implements Runnable{
        public volatile boolean canceled = false;
        BlockingQueue storage ;
        public  Producer(BlockingQueue storage){
            this.storage = storage;
        }

        @Override
        public void run() {
            int num = 0;
            try {
                while (num <= 100000 && !canceled){
                    if (num %50 == 0 ){
                        storage.put(num);
                        System.out.println(num + "是50的倍数，被放到仓库中了。");
                    }
                    num++;
                }

            }catch (InterruptedException e){
                e.printStackTrace();
            }finally {

            }

        }
    }
   static class Consumer{
        BlockingQueue storage;

        public Consumer(BlockingQueue storage){
            this.storage = storage;
        }
        public boolean needMoreNums(){
            if (Math.random() > 0.97){
                return false;
            }
            return true;
        }
    }

    public static void main(String[] args) throws InterruptedException {
//        用interrupt停止线程
//        Thread thread = new Thread(new StopThread());
//        thread.start();
//        Thread.sleep(5);
//        thread.interrupt();


//        //为什么用volatile标记位的停止方法是错误的
//        ArrayBlockingQueue storage = new ArrayBlockingQueue(8);
//
//        Producer producer = new Producer(storage);
//        Thread producerThread = new Thread(producer);
//        producerThread.start();
//        Thread.sleep(500);
//
//        Consumer consumer = new Consumer(storage);
//        while (consumer.needMoreNums()){
//            System.out.println(consumer.storage.take()+"被消费了");
//            Thread.sleep(100);
//        }
//        System.out.println("消费者不需要更多数据了");
//
//        //一旦消费不需要更多数据了，我们应该让生产者也停下来，但是实际情况
//        producer.canceled = true;
//        System.out.println(producer.canceled);
    }
}
