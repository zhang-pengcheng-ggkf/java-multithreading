package JavaConcurrent_1;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Concurrent_1_4 {

  //使用Condition实现生产者消费者模式
    private Queue queue;
    private int max = 16;
    private ReentrantLock lock = new ReentrantLock();
    private Condition notRmpty = lock.newCondition();
    private Condition notFull = lock.newCondition();


    public Concurrent_1_4(int size){
        this.max =size;
        queue = new LinkedList();
    }

    public void put(Object o) throws InterruptedException{
        lock.lock();
        try {
            while(queue.size() == max)
            {
                notFull.await();
            }
            queue.add(o);
            notRmpty.signalAll();
        }finally {
            lock.unlock();
        }
    }
    public Object take() throws InterruptedException{
        lock.lock();
        try{
            while(queue.size() == 0){
                notRmpty.await();
            }
            Object item = queue.remove();
            notFull.signalAll();
            return item;
        }finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {

    }
}

//使用wait和notify实现生产者和消费者
class MyBlockingQueue{
    private  int maxSize;
    private LinkedList<Object> storage;

    public MyBlockingQueue(int size){
        this.maxSize = size;
        storage = new LinkedList<>();
    }
    public synchronized void put() throws InterruptedException{
        while (storage.size() == maxSize){
            wait();
        }
        storage.add(new Object());
        notifyAll();
    }
    public synchronized void take() throws InterruptedException{
        while (storage.size() == 0){
            wait();
        }
        System.out.println(storage.remove());
        notifyAll();
    } 
}
//使用MyBlockingQueue实现生产者消费者
class WaitStyle{
    public static void main(String[] args) {
        MyBlockingQueue myBlockingQueue = new MyBlockingQueue(10);
        Producer producer = new Producer(myBlockingQueue);
        Consumer consumer = new Consumer(myBlockingQueue);
        new Thread(producer).start();
        new Thread(consumer).start();
    }

}
class Producer implements Runnable{
    private MyBlockingQueue storage;
    public Producer(MyBlockingQueue storage){
        this.storage = storage;
    }

    @Override
    public void run() {
        for (int i = 0; i<100;i++){
            try{
                storage.put();
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
class Consumer implements Runnable{
    private MyBlockingQueue storge;
    public Consumer(MyBlockingQueue storage){
        this.storge = storage;
    }

    @Override
    public void run() {
        for (int i = 0; i<100;i++){
            try{
                storge.take();
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
