package JavaConcurrent_1;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Concurrent_1_1 {

    //通过实现Runnable接口创建线程
    static class RunnableThread implements Runnable{

        @Override
        public void run() {
            System.out.println("通过实现Runnable接口实现多线程");
        }
    }

    //继承Thread类创建线程
    static class ExtendsThread extends Thread{
        @Override
        public void run() {
            System.out.println("继承Thread类实现多线程");
        }
    }

    //使用线程池创建线程
    static class DefaultThreadFactory implements ThreadFactory {

        private static final AtomicInteger poolNumber = new AtomicInteger(1);
        private final ThreadGroup group;
        private final AtomicInteger threadNumber = new AtomicInteger(1);
        private final String namePrefix;

        DefaultThreadFactory() {
            SecurityManager s = System.getSecurityManager();
            group = (s != null) ? s.getThreadGroup() :
                    Thread.currentThread().getThreadGroup();
            namePrefix = "pool-" +
                    poolNumber.getAndIncrement() +
                    "-thread-";
        }

        public Thread newThread(Runnable r) {
            Thread t = new Thread(group, r,
                    namePrefix + threadNumber.getAndIncrement(),
                    0);
            if (t.isDaemon())
                t.setDaemon(false);
            if (t.getPriority() != Thread.NORM_PRIORITY)
                t.setPriority(Thread.NORM_PRIORITY);
            return t;
        }
    }

    //使用Callable实现线程
    static class CallableTask implements Callable<Integer>{

        @Override
        public Integer call() throws Exception {
            return new Random().nextInt();
        }
    }

    //定时器Timer实现多线程
    static class MyTask extends TimerTask {

        @Override
        public void run() {
            System.out.println("定时器Timer实现多线程");
        }
    }



    public static void main(String[] args) {

        //通过实现Runnable接口创建线程
        new RunnableThread().run();

        //继承Thread类创建线程
        new ExtendsThread().start();


        //创建线程池
        ExecutorService service = Executors.newFixedThreadPool(10);
        //提交任务，并用Future提交返回结果
        Future<Integer> future = service.submit(new CallableTask());

        //创建定时器对象
        Timer t=new Timer();
        //在3秒后执行MyTask类中的run方法
        t.schedule(new MyTask(), 3000);

        //匿名内部类创建线程
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        }).start();

        //使用lambda创建线程
        new Thread(()->
                System.out.println(Thread.currentThread().getName())).start();

    }





}
