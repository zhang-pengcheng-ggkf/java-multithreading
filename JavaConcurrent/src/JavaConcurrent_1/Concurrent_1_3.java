package JavaConcurrent_1;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

//wait必须在synchronized保护的同步代码块中使用
public class Concurrent_1_3 {
    BlockingQueue buffer ;
    //生产者
    public void give(String data ){
        synchronized (this){
            buffer.add(data);
        }
    }

    //消费者
    public String take() throws InterruptedException{
        synchronized (this){
            while (buffer.isEmpty()){
                wait();
            }
            return (String) buffer.remove();//若没有synchronized修饰，很可能会被虚假唤醒
        }
    }
}
