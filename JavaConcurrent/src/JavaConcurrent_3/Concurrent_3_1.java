package JavaConcurrent_3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Concurrent_3_1 {
    //为什么要使用线程池
    public static class TenTask {
        public static void main(String[] args) {
            for (int i = 0; i < 10; i++) {
                Thread thread = new Thread(new Task());
                thread.start();
            }
        }
    }

    //用固定线程数的线程池执行10000个任务
    public static class ThreadPoolDemo {
        public static void main(String[] args) {
            ExecutorService service = Executors.newFixedThreadPool(5);
            for (int i = 0; i < 10000; i++)
                service.execute(new Task());
        }
    }


    static class Task implements Runnable {

        @Override
        public void run() {
            System.out.println("Thread Name: " + Thread.currentThread().getName());
        }
    }

}
