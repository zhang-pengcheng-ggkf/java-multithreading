package JavaConcurrent_2;

import java.util.HashMap;
import java.util.Map;

public class Concurrent_2_2 {
    static int i;

    public static void main(String[] args) throws InterruptedException {


//        //访问共享变量和资源
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                for (int j = 0 ; j < 10000 ; j++)
//                {
//                    //i++并不是一个原子操作，执行步骤为 1.读取 2.增加 3.保存
//                    //i++可能会在thread1增加的过程中被thread2拿去读取
//                    i++;
//                }
//            }
//        };
//        Thread thread1 = new Thread(runnable);
//        thread1.start();
//        Thread thread2 = new Thread(runnable);
//        thread2.start();
//        thread1.join();
//        thread2.join();
//        System.out.println(i);

    }

//    //依赖时序的操作
//   if(map.containsKey(key)){
//       //假如第一个线程判断完后去执行另一个任务，而第二个线程来判断并执行删除，当第一个线程继续执行就会发现值为null
//       map.remove(obj);
//    }
}
