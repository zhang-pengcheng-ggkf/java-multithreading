package JavaConcurrent_2;

import java.util.HashMap;
import java.util.Map;

public class Concurrent_2_1 {

    static int i;

    public static void main(String[] args) throws InterruptedException {


//        //运行结果错误
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                for (int j = 0 ; j < 10000 ; j++)
//                {
//                    //i++并不是一个原子操作，执行步骤为 1.读取 2.增加 3.保存
//                    //i++可能会在thread1增加的过程中被thread2拿去读取
//                    i++;
//                }
//            }
//        };
//        Thread thread1 = new Thread(runnable);
//        thread1.start();
//        Thread thread2 = new Thread(runnable);
//        thread2.start();
//        thread1.join();
//        thread2.join();
//        System.out.println(i);


//       //发布和初始化导致线程安全问题
//        //students这个成员变量是在构造函数中新建线程中进行初始化和赋值操作，而线程启动需要一定的时间，但是我们的main函数没有等待就进行获取数据，导致结果为null
//        Concurrent_2_1 multiThreadError6 = new Concurrent_2_1();
//        System.out.println(multiThreadError6.getStudents().get(1));

    }

    //发布和初始化导致线程安全问题
    private Map<Integer, String> students;

    public Concurrent_2_1() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                students = new HashMap<>();
                students.put(1, "王小美");
                students.put(2, "钱二宝");
                students.put(3, "周三");
                students.put(4, "赵四");

            }
        }).start();
    }

    public Map<Integer, String> getStudents() {
        return students;
    }

    //活跃性问题--死锁
    public static class MayDeadLock {
        Object object1 = new Object();
        Object object2 = new Object();

        public void thread1() throws InterruptedException {
            synchronized (object1) {
                Thread.sleep(500);
                synchronized (object2) {
                    System.out.println("线程一成功拿到了两把锁");
                }
            }
        }

        public void thread2() throws InterruptedException {
            synchronized (object2) {
                Thread.sleep(500);
                synchronized (object1) {
                    System.out.println("线程二成功拿到了两把锁");
                }
            }
        }

        public static void main(String[] args) {
            MayDeadLock mayDeadLock = new MayDeadLock();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        mayDeadLock.thread1();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        mayDeadLock.thread2();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
}
