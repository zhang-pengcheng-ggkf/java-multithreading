package JavaConcurrent_4;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
//公平锁和非公平锁的案列
public class JavaConcurrent_4_1 {

    public static void main(String[] args) {
        PrintQueue printQueue = new PrintQueue();
        Thread thread[] = new Thread[10];
        for (int i=0; i<10;i++){
            thread[i] = new Thread(new Job(printQueue),"Thread"+i);
        }

        for (int i=0;i<10;i++)
        {
            thread[i].start();
            try {
                Thread.sleep(100);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
class Job implements Runnable{

    private PrintQueue printQueue;

    public Job(PrintQueue printQueue){
        this.printQueue = printQueue;
    }
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + ": Going to print a job\n");
        printQueue.printJob(new Object());
        System.out.println( Thread.currentThread().getName()+": The document has printed\n");
    }
}
class PrintQueue{
    //设置是否为公平锁
    private final Lock queueLock = new ReentrantLock(true);

    public void printJob(Object document){
        queueLock.lock();

        try {
            long duraton = (long)(Math.random() * 10000);
            System.out.println(Thread.currentThread().getName()+ ": PrintQueue: Printing a Job during " + (duraton/1000)+"sencond\n");
            Thread.sleep(duraton);

        }catch (InterruptedException e){
            e.printStackTrace();
        }finally {
            queueLock.unlock();
        }
    }
}
