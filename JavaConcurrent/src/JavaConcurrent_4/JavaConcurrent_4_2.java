package JavaConcurrent_4;

import java.util.concurrent.locks.ReentrantReadWriteLock;

//读写锁规则
public class JavaConcurrent_4_2 {

    private static final ReentrantReadWriteLock reentratReadWriteLock = new ReentrantReadWriteLock(false);

    private static final ReentrantReadWriteLock.ReadLock readLock = reentratReadWriteLock.readLock();

    private static final ReentrantReadWriteLock.WriteLock writeLock = reentratReadWriteLock.writeLock();


    private  static void read(){
        readLock.lock();
        try{
            System.out.println(Thread.currentThread().getName()+"得到读锁，正在读锁");
            Thread.sleep(500);
        }catch (InterruptedException e){
            e.printStackTrace();
        }finally {
            readLock.unlock();
            System.out.println(Thread.currentThread().getName()+"释放读锁");
        }
    }

    private static void write(){
        writeLock.lock();
        try {
            System.out.println(Thread.currentThread().getName()+"得到写锁，正在写锁");
            Thread.sleep(500);
        }catch (InterruptedException e){
            e.printStackTrace();
        }finally {
            writeLock.unlock();
            System.out.println(Thread.currentThread().getName()+"释放写锁");
        }
    }

    public static void main(String[] args) {
        new Thread(()->read()).start();
        new Thread(()->read()).start();
        new Thread(()->write()).start();
        new Thread(()->write()).start();
    }
}
